import time
from flask import Flask, Response
from datetime import datetime, timedelta

app = Flask(__name__)

@app.route('/')
def index():
    return "Time Generator is running..."

@app.route('/clock')
def get_time_for_web():
    def eventStream():
        while True:
            yield get_time()
    return Response(eventStream(), status=200, mimetype='text/event-stream')

def get_time():
    """this could be any function that blocks until data is ready"""
    time.sleep(1.0)
    s = time.ctime(time.time())
    return s

def bootapp():
    app.run(port=8080, threaded=True, host=('0.0.0.0'))

if __name__ == '__main__':
     bootapp()
